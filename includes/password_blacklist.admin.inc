<?php

// $Id$

/**
 * Define the password_blacklist_settings_form() function.
 */
function password_blacklist_settings_form() {
  $form = array();

  $form['password_blacklist_entry'] = array(
    '#type' => 'textfield',
    '#description' => t('For multiple entries you can use comma separated values. "12345, god, love, etc..."'),
    '#required' => TRUE,
  );
  $form['password_blacklist_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['#submit'][] = 'password_blacklist_settings_form_submit';

  return $form;
}

/**
 * Define the password_blacklist_settings_form_submit() function.
 */
function password_blacklist_settings_form_submit($form, &$form_state) {
  $found = '';
  $table = 'password_blacklist';
  $object = new stdClass;
  $entries = explode(',', $form_state['values']['password_blacklist_entry']);
  foreach ($entries as $key => $value) {
    $entry = str_replace(' ', '', $value);
    $query = db_query("SELECT pid FROM {password_blacklist} WHERE value = '%s'", $entry);
    $result = db_result($query);
    if (!$result) {
      $object->value = $entry;
      drupal_write_record($table, $object);
    }
    else {
      $found .= "$entry, ";
    }
  }
  if (!empty($found)) {
    drupal_set_message(t('The following passwords are already in the blacklist. "@found"', array('@found' => $found)), 'warning');
  }
  drupal_set_message('Configuration has been saved.');
}

/**
 * Define the theme_password_blacklist_settings_form() function.
 */
function theme_password_blacklist_settings_form($form) {
  $header = array(t('Value'), t('Operations'));
  $query = db_query("SELECT * FROM {password_blacklist}");
  while ($result = db_fetch_array($query)) {
    $rows[] = array($result['value'], t('delete'));
  }
  $rows[] = array(drupal_render($form['password_blacklist_entry']), drupal_render($form['password_blacklist_submit']));

  $output = drupal_render($form);
  $output .= theme('table', $header, $rows);

  return $output;
}
